# Tailwind CSS & Next.js Starter Kit

This is a lightweight starterkit / boilerplate to get up and running with Tailwind CSS and Next.js very quickly. Included is a sample now.json configuration file to make deploying to [Now](https://zeit.co/now) very easy.

## Why make this boilerplate?

I personally build a lot of web apps for companies and I enjoy using Tailwind CSS and Next.js. I just got really tired of the initial boilerplate setup, so I decided to make this repo so I can get up and running with a new project very easily.

## Usage

1. Run `git clone https://github.com/tracy-codes/tailwindcss-nextjs-starter <your-project-name>` to clone this repo to a new folder. Replace `<your-project-name>` with whatever you want to title your project folder.
2. Run `cd <your-project-name>`.
3. Run `yarn install` or `npm install`.
4. Run `yarn run dev` or `npm run dev` to start your development environment.
5. Run `yarn run build` or `npm run build` to build your source code.

## Deploying to Now

1. Run through the [Now Installation](https://zeit.co/docs/v2/getting-started/installation/).
2. Modify the file `now.json` to suit your needs.
3. Run `now` to deploy to your Now account.

## Important Notes

- Be sure to update the `package.json` to include your own information for the project you're working on.
